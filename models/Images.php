<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property int $image_id
 * @property int $product_id
 * @property string $image_path
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'image_path'], 'required'],
            [['product_id'], 'integer'],
            [['image_path'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'image_id' => 'Image ID',
            'product_id' => 'Product ID',
            'image_path' => 'Image Path',
        ];
    }
}
