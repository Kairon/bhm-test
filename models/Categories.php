<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $category_id
 * @property string $category_name
 * @property int $parent_category_id
 */
class Categories extends \yii\db\ActiveRecord
{
    public $count;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'parent_category_id'], 'required'],
            [['parent_category_id'], 'integer'],
            [['category_name'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'category_name' => 'Category Name',
            'parent_category_id' => 'Parent Category ID',
        ];
    }

    public function getProducts(){
        return $this->hasMany(Products::className(), ['id' => 'product_id'])->viaTable('category_products', ['category_id' => 'id']);
    }

    public function getCategoryProducts(){
        return $this->hasOne(CategoryProducts::className(), ['category_id' => 'id']);
    }
}
