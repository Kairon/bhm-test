<?php

namespace app\models;

use Yii;
use yii\helpers\BaseStringHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "products".
 *
 * @property int $product_id
 * @property int $brand_id
 * @property string $name
 * @property string $description
 * @property int $rate
 * @property double $price
 */
class Products extends \yii\db\ActiveRecord
{
    const RANDOM_PRODUCTS = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'name', 'description', 'price'], 'required'],
            [['brand_id', 'rate'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'brand_id' => 'Brand ID',
            'name' => 'Name',
            'description' => 'Description',
            'rate' => 'Rate',
            'price' => 'Price',
        ];
    }

    public function getCategories(){
        return $this->hasMany(Categories::className(), ['category_id' => 'category_id'])->viaTable('category_products', ['product_id' => 'product_id']);
    }

    public function getCategoryProducts(){
        return $this->hasMany(CategoryProducts::className(), ['product_id' => 'product_id']);
    }

    public function getShortDesc(){
        return BaseStringHelper::truncate($this->description, '300');
    }

    public function getImages(){
        return $this->hasMany(Images::className(), ['product_id' => 'product_id']);
    }

    public function getImagesStr(){
        $imgArr = [];
        foreach ($this->images as $image){
            $imgArr[] = Html::img(Yii::$app->homeUrl . 'images' . $image->image_path, ['width' => '30px']);
        }
        return implode('', $imgArr);
    }

    public function getRandomProducts(){
        $catIds_arr = [];
        $rand_cat = null;
        $result = [];
        foreach ($this->categoryProducts as $category) {
            $catIds_arr[] = $category->category_id;
        }
        shuffle($catIds_arr);
        $catIds_arr = array_slice($catIds_arr, 0, 2);

        // Выполняются дополнительные запросы
        /*$products = self::find()
            ->joinWith('categories')
            ->where(['categories.category_id' => $catIds_arr])
            ->limit(self::RANDOM_PRODUCTS)
            ->orderBy('RAND()')
            ->all();*/
        $products = self::find()
            ->leftJoin('category_products', 'category_products.product_id=products.product_id')
            ->leftJoin('categories', 'categories.category_id=category_products.category_id')
            ->where(['categories.category_id' => $catIds_arr])
            ->limit(self::RANDOM_PRODUCTS)
            ->orderBy('RAND()')
            ->all();

        foreach ($products as $product){
            $result[] = $product->name;
        }
        return Html::ul($result);
    }

    public function getComments(){
        return $this->hasMany(Comments::className(), ['product_id' => 'product_id']);
    }

    public function getCommentsList(){
        $arr = [];
        foreach ($this->getComments()->with('user')->all() as $comment){
            $arr[] = $comment->comment_text . " / " . $comment->user->user_name;
        }
        return Html::ul($arr);
    }

    public function getBrands(){
        return $this->hasOne(Brands::className(), ['brand_id' => 'brand_id']);
    }
}
