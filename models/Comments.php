<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property int $comment_id
 * @property int $product_id
 * @property int $user_id
 * @property string $comment_text
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'user_id', 'comment_text'], 'required'],
            [['product_id', 'user_id'], 'integer'],
            [['comment_text'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comment_id' => 'Comment ID',
            'product_id' => 'Product ID',
            'user_id' => 'User ID',
            'comment_text' => 'Comment Text',
        ];
    }

    public function getProduct(){
        return $this->hasOne(Products::className(), ['product_id' => 'product_id']);
    }

    public function getUser(){
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }
}
