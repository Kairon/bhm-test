<?php

namespace app\controllers;

use app\models\Comments;
use app\models\Products;
use yii\db\Expression;

class CommentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = Products::find()
            ->select(['products.*', 'MAX(comments.product_id)'])
            ->leftJoin('comments', 'comments.product_id=products.product_id')
            ->one();
        return $this->render('index', [
            'model' => $model,
        ]);
    }

}
