<?php

namespace app\controllers;

use app\models\Categories;
use app\models\CategoryProducts;
use app\models\Products;
use yii\data\ActiveDataProvider;

class CategoryController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = Categories::find()
            ->select(['categories.category_name', 'COUNT(category_products.category_id) as `count`'])
            ->join('LEFT OUTER JOIN','category_products', 'category_products.category_id=categories.category_id')
            ->groupBy('categories.category_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionView($id){
        $query = Products::find()
            ->with(['images'])
            ->joinWith('categories')
            ->where(['categories.category_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('view', [
            'dataProvider' => $dataProvider
        ]);
    }

}
