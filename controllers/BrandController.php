<?php

namespace app\controllers;

use app\models\Brands;
use yii\data\ActiveDataProvider;

class BrandController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = Brands::find()
            ->select(['brands.*', 'COUNT(comments.comment_id) AS count'])
            ->leftJoin('products', 'products.brand_id = brands.brand_id')
            ->leftJoin('comments', 'comments.product_id = products.product_id')
            ->groupBy('brands.brand_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

}
