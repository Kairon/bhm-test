<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>
<h1>category/index</h1>

<p>
    <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'category_name',
                'count',
            ]
    ]); ?>
</p>
