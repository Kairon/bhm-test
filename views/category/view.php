<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>
<h1>category/view</h1>

<p>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            'price',
            'shortDesc',
            'imagesStr:html',
        ]
    ])?>
</p>
