<?php
/* @var $this yii\web\View */
/* @var $model \app\models\Products */
?>
<h1>comment/index</h1>

<p>
    <?= \yii\widgets\DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'commentsList:html'
        ]
    ]); ?>
</p>
