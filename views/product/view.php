<?php
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
?>
<h1>product/view/id = <?=$model->product_id?></h1>

<p>
    <?= \yii\widgets\DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'price',
            'description',
            'imagesStr:html',
            'commentsList:html',
            'randomProducts:html',
        ]
    ]) ?>
</p>
